# This is an example python script.

#In python comments are marked with # and new lines of code are indicated by a new line in the script


print("Hello World") # a classic introduction

a = 7
b = 6

print(a*b)

# Unlike compiled code, python will fail at runtime if there is an issue, try changing the below block to cause an error

print(3/0.1)