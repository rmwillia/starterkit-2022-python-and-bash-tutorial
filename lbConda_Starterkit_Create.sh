#!/bin/bash

source $HOME/.bashrc
shopt -s expand_aliases

(source /cvmfs/lhcb.cern.ch/lib/LbEnv 
lb-conda-dev virtual-env default/2022-11-21 $1
$1/run pip install zfit==0.10.1
$1/run python -m ipykernel install --user --name=$1
) &> starterkitWrapperInstall.txt