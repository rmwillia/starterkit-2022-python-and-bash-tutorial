# Starterkit 2022-python-and-bash-tutorial

This is a repo containing the material for the bash/python lesson during [Starterkit 2022](https://indico.cern.ch/event/1206471/timetable/?view=standard).

Structure:

- lbConda_Starterkit_Create.sh : A short bash script to install a virtual lb-conda environment with zFit installed and create an ipykernel for jupyter access!

- PythonExample.py : A very short example to show starterkitters how to run python from the command line.

- Python Reminder Notebook.ipynb : A short reminder notebook with some reminders for some python concepts, also serving as an example to show how to use jupyter!

- notebooks : The notebooks for the main Advanced Python Lessons